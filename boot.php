<?php
// For debugging purposes ...
error_reporting(E_ALL);

if(file_exists(".theme") && !is_dir(".theme"))
    $theme = fgets(fopen(".theme", "r"));

// Set user configurations and global variables
include "config.php";

// Load helper functions.
include_once LIB_PATH . "helpers.php";

// Set system locale
setlocale(LC_ALL, SYSTEM_LOCALE);
date_default_timezone_set(SYSTEM_TIMEZONE);
// Load Phostgre (http://github.com/seckin92/phostgre/)
//include LIB_PATH . "phostgre/load.php";

// Load Loggy (http://github.com/seckin92/loggy/)
include LIB_PATH . "loggy/load.php";

// Load Registry class.
include LIB_PATH . "registry.class.php";

// Load classes.
$classes = scandir(LIB_PATH);
foreach($classes as $class) {
    if(strpos($class, ".class.php")) {
        $parts = explode('.', $class);
        $class_name = $parts[0];        
        if(!class_exists($class_name)) {
            include LIB_PATH . $class;
        }
    }
}
unset($classes);

// Load models.
$models = scandir(MODEL_PATH);
foreach($models as $model) {
    if(strpos($model, ".model.php")) {
        $parts = explode('.', $model);
        $model_name = $parts[0];        
        if(!class_exists($model_name)) {
            include MODEL_PATH . $model;
        }
    }
}
unset($models);

// Instantiate loggy.
$loggy = new Loggy(LOG_PATH . "log.gy");

// Instantiate phostgre engine.
//$engine = new Engine();