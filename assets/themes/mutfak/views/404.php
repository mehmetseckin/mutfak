<div class="wrapper">
<!-- www.mku.edu.tr style header begin -->
<div class="header_bg">
    <div class="icerik_div">
        
        <div class="logo_div">
            <a href="http://www.mku.edu.tr/"><img src="<?php echo PROJECT_ROOT . IMG_PATH; ?>logotr.png" alt=""></a>
        </div>

        <div class="mutfak-logo">
            <a href="<?php echo PROJECT_ROOT; ?>"><img src="<?php echo PROJECT_ROOT . IMG_PATH; ?>mutfak-logo.png" alt=""></a>
        </div>
        
        <div class="clear"></div>        
    </div>
</div>
<!-- www.mku.edu.tr style header end -->
<div id="nav-bar">

</div>
<div class="content">
<div class="missing-404-container">
    <img src="<?php echo PROJECT_ROOT . IMG_PATH; ?>error-404.png" id="missing-404"/>    
</div>
<div class="error">
    <h1>Bir tane eksik gibi görünüyor... :(</h1>
    <hr>
    <h2>Emin olmak için iki kere baktık ama, aradığınız şey mutfakta değil..</h2>
    <h4>Lütfen <a href="<?php echo PROJECT_ROOT; ?>">Anasayfa'ya dönün.</a></h4>
    
</div>