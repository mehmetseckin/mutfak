<?php
$loggy = new Loggy(LOG_PATH . "log.gy");
if(!is_null($page->get("truncate"))) {
    $loggy->truncate();
    $loggy->w("Log file truncated by the user.", "Loggy");
}
echo $loggy->export();
?>
<hr>
Log dosyasını temizlemek için <a href="truncate">tıklayın.</a>
<script type="text/javascript">
$(document).ready(function() {
   loggyTable = $("#loggy-entries").dataTable({
       "bJQueryUI": true,     
       "iDisplayLength": 50
   });
});
</script>