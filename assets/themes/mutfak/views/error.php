<div class="wrapper">
<!-- www.mku.edu.tr style header begin -->
<div class="header_bg">
    <div class="icerik_div">
        
        <div class="logo_div">
            <a href="http://www.mku.edu.tr/"><img src="<?php echo PROJECT_ROOT . IMG_PATH; ?>logotr.png" alt=""></a>
        </div>

        <div class="mutfak-logo">
            <a href="<?php echo PROJECT_ROOT; ?>"><img src="<?php echo PROJECT_ROOT . IMG_PATH; ?>mutfak-logo.png" alt=""></a>
        </div>
        
        <div class="clear"></div>        
    </div>
</div>
<!-- www.mku.edu.tr style header end -->
<div id="nav-bar">

</div>
<div class="content">
<div class="broken-glass-container">
    <img src="<?php echo PROJECT_ROOT . IMG_PATH; ?>error-broken-glass.png" id="broken-glass"/>
</div>
<div class="error">
    <h1>Eyvah!</h1>
    <h2>Bir şeyler çok fena ters gitti..</h2>
    <hr>
    <h3>Merak etmeyin, bu durum <a href="<?php echo PROJECT_ROOT; ?>/logs/">sistem günlüğü</a>ne kaydedildi.</h3>
    <h4>Lütfen <a href="<?php echo PROJECT_ROOT; ?>">Anasayfa'ya dönün.</a></h4>
</div>