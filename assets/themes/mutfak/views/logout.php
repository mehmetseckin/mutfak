<?php
if($session->id) {
    $session->destroy();
}
header('Location: '. PROJECT_ROOT);