<?php
if(empty($_POST) || $session->id) {
    header('Location: '. PROJECT_ROOT);
} else {
    $kullanici_adi = $_POST["kullanici_adi"];
    $parola        = md5($_POST["parola"]);
    $user = new Kullanici();
    if(!$user->login($kullanici_adi, $parola)) {
        echo $user->lastError();
    }
    else {
        echo true;
    }
}

?>