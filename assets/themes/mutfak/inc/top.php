<div class="wrapper">
<!-- www.mku.edu.tr style header begin -->
<div class="header_bg">
    <div class="icerik_div">
        
        <div class="logo_div">
            <a href="http://www.mku.edu.tr/"><img src="<?php echo PROJECT_ROOT . IMG_PATH; ?>logotr.png" alt=""></a>
        </div>

        <div class="mutfak-logo">
            <a href="<?php echo PROJECT_ROOT; ?>"><img src="<?php echo PROJECT_ROOT . IMG_PATH; ?>mutfak-logo.png" alt=""></a>
        </div>
        
        <div class="clear"></div>        
    </div>
</div>
<!-- www.mku.edu.tr style header end -->
<div id="nav-bar">
       <div class="menu">
        <ul>
        <?php foreach($menu as $item) { ?>
           <li <?php if(isset($item["current"])) { ?> class="current" <?php } ?>>
           <a href="<?php echo $item["href"]; ?>"><?php echo $item["title"]; ?></a></li>
       <?php } ?>
        </ul>
        <div class="hotlinks">
            <ul>
                <?php if($session->id) { ?>
                <li><a href="<?php echo PROJECT_ROOT; ?>users/<?php echo $user->kullanici_adi; ?>/">@<?php echo $user->kullanici_adi; ?></a></li>
                <li><a href="<?php echo PROJECT_ROOT; ?>logout/">Çıkış</a></li>
                <?php } else { ?>
                <li><a id="login-link">Giriş</a></li>
                <div class="login-box">
                    <form name="login-form" id="login-form" method="post" action="login">
                    <input type="text" placeholder="Kullanıcı Adı" name="kullanici_adi" />
                    <input type="password" placeholder="Parola" name="parola" />
                    <input type="checkbox" name="beni-hatirla" id="beni-hatirla" checked><label for="beni-hatirla">Beni Hatırla</label>                    
                    <input type="button" name="login" id="login" value="Giriş Yap" />
                    </form>
                    <div id="login-message"></div>
                </div>
                <?php } ?>
            </ul>
         </div>
       </div>
</div>
<div class="content">