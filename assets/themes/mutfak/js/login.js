$(document).ready(function() {
    $("#login-link").click(function() {
        $(".login-box").fadeToggle();
    });
    
    $("#login").click(function() {
       $("#login-form").submit(); 
    });
    
    /* Initializing submission */
    $(function() {
        $("form input").keypress(function (e) {
            if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                $('#login').click();
                return false;
            } else {
                return true;
            }
        });
    });
    
    //callback handler for form submit
    $("#login-form").submit(function(e)
    {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            data : postData,
            success:function(data, textStatus, jqXHR) 
            {
                if(data==true) {
                    $('.login-box').animate({ backgroundColor: "#26E626" }, "normal");   
                    $('.login-box').fadeOut();
                    location.reload();
                }
                else {
                    $('#login-message').html(data);
                    $('.login-box').animate({ backgroundColor: "#E62626" }, "normal");
                    $('.login-box').animate({ backgroundColor: "#F2C695" }, "normal");                    
                }
            }
        });
        e.preventDefault(); //STOP default action
    });
 
});