<?php
$_["stylesheets"]   =   array(
                            array("media" => "screen", "href" => "reset.css"),
                            array("media" => "screen", "href" => "main.css"),
                            array("media" => "screen", "href" => "jquery.dataTables.css"),
                            array("media" => "screen", "href" => "themes/jquery-ui-lightness/jquery-ui-lightness.css")
                        );   
$_["scripts"]       =   array(
                          array("type" => "text/javascript", "src" => "jquery.js"),
                          array("type" => "text/javascript", "src" => "jquery.ui.js"),
                          array("type" => "text/javascript", "src" => "jquery.dataTables.js")
                        );
$_["hasHeader"]     = true;
$_["menu"]          =   array(
                            array("href" => PROJECT_ROOT, "title" => "Anasayfa"),
                            array("href" => PROJECT_ROOT . "logs/", "title" => "Sistem Günlüğü", "current" => true)
                        );
$_["title"]         =   "Sistem Günlüğü | Mutfak : MKÜ Mühendislik Fakültesi Proje Portalı";