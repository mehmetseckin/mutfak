<?php
$_["stylesheets"]   =   array(
                            array("media" => "screen", "href" => "reset.css"),
                            array("media" => "screen", "href" => "main.css")
                        );   
$_["scripts"]       =   array(
                          array("type" => "text/javascript", "src" => "jquery.js"),
                          array("type" => "text/javascript", "src" => "jquery.color.js"),
                          array("type" => "text/javascript", "src" => "jquery.shake.js"),
                          array("type" => "text/javascript", "src" => "main.js"),
                          array("type" => "text/javascript", "src" => "login.js")
                        );
$_["hasHeader"]     =   true;

$_["menu"]          =   array(
                            array("href" => PROJECT_ROOT, "title" => "Anasayfa", "current" => true),
                            array("href" => PROJECT_ROOT . "logs/", "title" => "Sistem Günlüğü")
                        );

$_["title"]         =   "Mutfak : MKÜ Mühendislik Fakültesi Proje Portalı";
