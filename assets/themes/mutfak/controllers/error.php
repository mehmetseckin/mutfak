<?php
// The CSS files to import.
$_["stylesheets"]   =   array(
                            array("media" => "screen", "href" => "reset.css"),
                            array("media" => "screen", "href" => "main.css"),
                        );   

// The JS files to import.
$_["scripts"]       =   array(
                          array("type" => "text/javascript", "src" => "jquery.js")
                        );
// Menu items
$_["menu"]          =   array(

                        );
// Page title
$_["title"]         =   "Hata! | Mutfak : MKÜ Mühendislik Fakültesi Proje Portalı";