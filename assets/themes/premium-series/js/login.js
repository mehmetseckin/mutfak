$(document).ready(function() {
    $("#login-link").click(function() {
        $(".login-box").fadeToggle();
    });
    
    $("#login").click(function() {
       $("#login-form").submit(); 
    });
    
    /* Initializing submission */
    $(function() {
        $("form input").keypress(function (e) {
            if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                $('#login').click();
                return false;
            } else {
                return true;
            }
        });
    });
    
    //callback handler for form submit
    $("#login-form").submit(function(e)
    {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            data : postData,
            success:function(data, textStatus, jqXHR) 
            {
                try {
                    var response = JSON.parse(data);
                    if(response.success) {
                        $('.login-box').animate({ backgroundColor: "#5CB85C" }, "normal");   
                        $('.login-box').fadeOut();
                        location.reload();
                    }
                    else {
                        $('#login-message').html(response.message);
                        $('.login-box').animate({ backgroundColor: "#D9534F" }, "normal");
                        $('.login-box').animate({ backgroundColor: "transparent" }, "normal");                    
                    }
                }
                catch (Exception) {
                    console.log("Output : " + data);
                    $('#login-message').html("Bir hata oluştu.");
                    $('.login-box').animate({ backgroundColor: "#D9534F" }, "normal");
                    $('.login-box').animate({ backgroundColor: "transparent" }, "normal"); 
                }
            }
        });
        e.preventDefault(); //STOP default action
    });
 
});