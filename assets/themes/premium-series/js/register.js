$(document).ready(function() {
    // sayfa duzenini tekrar ayarla (no sidebars)
    $("#content").css({"width": "90%"});
    $("#page").css({width: "1000px"});
    $("#footer").css({width: "1000px"});
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Bu alan boş bırakılamaz!");
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }
});