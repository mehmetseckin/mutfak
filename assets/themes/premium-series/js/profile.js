$(document).ready(function() {

    $('[description]').each(function() {
        var description = $(this).attr("description");

        $(this).hover(
                function() {
                    $('#' + description).stop(true, true).fadeIn();
                },
                function() {
                    $('#' + description).stop(true, true).fadeOut();
                });
    });
});