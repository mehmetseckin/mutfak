<?php
$post = new RequestManager("POST");
if(empty($_POST) || $session->id) {
    redirect();
    exit;
} else {
    $kullanici_adi = $post->kullanici_adi;
    $parola        = md5($post->parola);
    $user = new Kullanici();
    if(!$user->login($kullanici_adi, $parola)) {
        echo json_encode(array(
            "message" => "Girdiğiniz bilgilerde bir yanlışlık olmalı.", 
            "success" => false
            ));
    }
    else {
        echo json_encode(array(
            "success" => true
            ));
    }
}

?>