<?php
if(!isset($_GET["user"])) redirect();

$u = $_GET["user"];

$profile = new Kullanici();
$profile->get($u, "kullanici_adi");
extract($profile->data->all());
?>
<div class="sidebar" style="text-align: center;">
    <img class="avatar" src="<?php echo ($avatar) ? PROJECT_ROOT . IMG_PATH . 'avatars/'. $avatar : PROJECT_ROOT . IMG_PATH . 'no_avatar.jpg'; ?>" />
    <span style="display: block; font-style: italic; padding: 3px;"><?php echo isset($unvan) ? $unvan : ""; ?></span>    
    <h1 style="padding: 3px;"><a href="<?php echo PROJECT_ROOT . 'profile/user/' . $kullanici_adi;?>"><?php echo $ad_soyad; ?></a></h1>
    <span style="font-style: italic; padding: 3px;" description="grup_aciklama"><?php echo $grup_adi; ?></span>
    <div class="hover-description-bottom" id="grup_aciklama"><?php echo $grup_aciklama; ?></div>
</div>    
<div class="post">
    <ul>
        <li>E-Posta adresi : <a href="mailto:<?php echo $e_mail; ?>"><?php echo $e_mail; ?></a></li>
        <li><?php echo date("d F Y", strtotime($kayit_tarihi)); ?> tarihinde katıldı.</li>
        <li>En son <?php echo date("d F l, H:i", strtotime($son_gorulme)); ?> tarihinde görüldü.</li>
    </ul>
</div>