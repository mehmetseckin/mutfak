<?php
if($session->id) {
    header("Location: ". PROJECT_ROOT);
    exit;
}

if($post->register) {
    $user = new Kullanici();
    
    $user->data->kullanici_adi = $post->kullanici_adi;
    $user->data->e_mail = $post->e_mail;
    $user->data->ogrenci_no = $post->ogrenci_no;
    $user->data->isim = $post->isim;
    $user->data->soyisim = $post->soyisim;
    $user->data->grup_id = $post->grup_id;
    $user->data->bolum_kodu = $post->bolum;
    $user->data->parola = md5($post->sifre);
    $user->data->aktif = 0;
    $user->data->kayit_tarihi = date('Y-m-d H:i:s');
    
    if($user->register()) {
        alert('Kaydınız başarıyla tamamlandı!', 'success');
    } else {
        alert('Kaydınız tamamlanamadı!', 'danger');
    }
} else { 

    $grup = new Grup();
    $grup_id = ($grup->get("Aşçılar", "grup_adi")) ? $grup->data->id : 3;
    
?>
<form class="form-horizontal" method="post" action="<?php echo PROJECT_ROOT . 'register/' ?>">
<fieldset>

<!-- Form Name -->
<legend><a style="padding-left: 10px;">Kayıt Ol</a></legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-5 control-label" for="ogrenci_no">Öğrenci Numaranız</label>  
  <div class="col-md-5">
  <input id="ogrenci_no" name="ogrenci_no" type="text" placeholder="120123123" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-5 control-label" for="bolum">Bölümünüz</label>
  <div class="col-md-5">
    <select id="bolum" name="bolum" class="form-control">
    <?php
        $b = new Bolum();
        $bolumler = $b->getList();
        foreach($bolumler as $bolum) {
            echo '<option value="'.$bolum["bolum_kodu"].'">'.$bolum["bolum_adi"].'</option>';
        }
    ?>   
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-5 control-label" for="isim">Adınız</label>  
  <div class="col-md-5">
  <input id="isim" name="isim" type="text" placeholder="Yakup" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-5 control-label" for="soyisim">Soyadınız</label>  
  <div class="col-md-5">
  <input id="soyisim" name="soyisim" type="text" placeholder="Kutlu" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-5 control-label" for="e_mail">E-Posta Adresiniz</label>  
  <div class="col-md-5">
  <input id="e_mail" name="e_mail" type="text" placeholder="posta@yakupkutlu.com" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-5 control-label" for="kullanici_adi">Kullanıcı Adınız</label>  
  <div class="col-md-5">
  <input id="kullanici_adi" name="kullanici_adi" type="text" placeholder="yakup_123" class="form-control input-md" required="">
  <span class="help-block">Kullanıcı adınız harfler, rakamlar ve altçizgiden oluşabilir. Türkçe karakter kullanmayınız.</span>  
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-5 control-label" for="sifre">Şifreniz</label>
  <div class="col-md-5">
    <input id="sifre" name="sifre" type="password" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-5 control-label" for="sifre_confirm"></label>
  <div class="col-md-5">
    <input id="sifre_confirm" name="sifre_confirm" type="password" placeholder="" class="form-control input-md" required="">
    <span class="help-block">Onaylamak için şifrenizi tekrar girin.</span>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-5 control-label" for="register"></label>
  <div class="col-md-5">
      <button type="submit" id="register" name="register" class="btn btn-danger" value="true">Kayıt Ol</button>
  </div>
</div>
<input type="hidden" name="grup_id" id = "grup_id" value="<?php echo $grup_id; ?>" />
</fieldset>
</form>
<?php } ?>