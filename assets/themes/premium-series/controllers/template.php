<?php
/**
 * An example controller file for the template view.
 */

// The CSS files to import.
$_["stylesheets"]   =   array(
                            array("media" => "screen", "href" => "reset.css"),
                            array("media" => "screen", "href" => "default.css"),
                        );   

// The JS files to import.
$_["scripts"]       =   array(
                          array("type" => "text/javascript", "src" => "jquery.js")
                        );

// The header flag, if set true, "include/top.php" will be included BEFORE the view.
$_["hasHeader"]     = true;
$_["hasFooter"]     = true;

// Menu items
$_["menu"]          =   array(
                            array("href" => PROJECT_ROOT, "title" => "Anasayfa"),
                            array("href" => PROJECT_ROOT . "logs/", "title" => "Sistem Günlüğü")
                        );
// Page title
$_["title"]         =   "Template";

// Some variable
$_["foo"] = "Hello, i am foo!";