<?php
$_["stylesheets"]   =   array(
                            array("media" => "screen", "href" => "reset.css"),
                            array("media" => "screen", "href" => "default.css")
                        );   
$_["scripts"]       =   array(
                          array("type" => "text/javascript", "src" => "jquery.js"),
                          array("type" => "text/javascript", "src" => "jquery.color.js"),
                          array("type" => "text/javascript", "src" => "jquery.shake.js"),
                          array("type" => "text/javascript", "src" => "main.js"),
                          array("type" => "text/javascript", "src" => "login.js"),
                          array("type" => "text/javascript", "src" => "profile.js")
                        );
$_["hasHeader"]     =   true;
$_["hasFooter"]     =   true;
$_["hasLeft"]       =   false;
$_["hasRight"]      =   true;

include("menu.php");

$_["title"]         =   "Proje Portalı | YakupKutlu.com";
