<?php
// The CSS files to import.
$_["stylesheets"]   =   array(
                            array("media" => "screen", "href" => "reset.css"),
                            array("media" => "screen", "href" => "default.css"),
                        );   

// The JS files to import.
$_["scripts"]       =   array(
                          array("type" => "text/javascript", "src" => "jquery.js")
                        );
// Menu items
include("menu.php");

$_["hasHeader"]     =   true;
$_["hasFooter"]     =   false;
$_["hasLeft"]       =   false;
$_["hasRight"]      =   false;
// Page title
$_["title"]         =   "404!";