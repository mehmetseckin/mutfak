<?php
// The CSS files to import.
$_["stylesheets"]   =   array(                            
                            array("media" => "screen", "href" => "reset.css"),
                            array("media" => "screen", "href" => "bootstrap.css"),
                            array("media" => "screen", "href" => "bootstrap-theme.css"),
                            array("media" => "screen", "href" => "default.css")    
                        );   

// The JS files to import.
$_["scripts"]       =   array(
                          array("type" => "text/javascript", "src" => "jquery.js"),
                          array("type" => "text/javascript", "src" => "bootstrap.js"),
                          array("type" => "text/javascript", "src" => "register.js")    
                        );
// Menu items
include("menu.php");

$_["hasHeader"]     =   true;
$_["hasFooter"]     =   true;
$_["hasLeft"]       =   false;
$_["hasRight"]      =   false;
// Page title
$_["title"]         =   "Kayıt ol | YakupKutlu.com";