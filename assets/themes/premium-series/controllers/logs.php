<?php
$_["stylesheets"]   =   array(
                            array("media" => "screen", "href" => "reset.css"),
                            array("media" => "screen", "href" => "default.css"),
                            array("media" => "screen", "href" => "jquery.dataTables.css"),
                            array("media" => "screen", "href" => "themes/jquery-ui-lightness/jquery-ui-lightness.css")
                        );   
$_["scripts"]       =   array(
                          array("type" => "text/javascript", "src" => "jquery.js"),
                          array("type" => "text/javascript", "src" => "jquery.ui.js"),
                          array("type" => "text/javascript", "src" => "jquery.dataTables.js")
                        );
$_["hasHeader"]     =   true;
$_["hasFooter"]     =   true;
$_["hasLeft"]       =   false;
$_["hasRight"]      =   false;

include("menu.php");

$_["title"]         =   "Sistem Günlüğü | Mutfak : MKÜ Mühendislik Fakültesi Proje Portalı";