<?php 
$session = new SessionManager(); 
$post = new RequestManager("POST");
$get = new RequestManager("GET");
?>
<html>
    <head>  
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title><?php echo $title; ?></title>
        <link rel="icon" href="<?php echo PROJECT_ROOT . IMG_PATH; ?>favicon.ico" type="image/x-icon" />
        <?php
        // Load all CSS files.
        if($stylesheets) {
            foreach ($stylesheets as $css) {
                echo '<link rel="stylesheet" media="' . $css["media"] . '" href="' . PROJECT_ROOT . CSS_PATH . $css["href"] . '"/>';
            }
        }
        ?>
        <!--[if gte IE 8]>
                    <link href="<?php echo PROJECT_ROOT . CSS_PATH;?>ie8plus.css" rel="stylesheet" type="text/css" />
            <![endif]-->

            <!--[if lt IE 8]>
                    <link href="<?php echo PROJECT_ROOT . CSS_PATH;?>ie7.css" rel="stylesheet" type="text/css" />
        <![endif]-->
        <!--[if lt IE 7]>
                    <link href="<?php echo PROJECT_ROOT . CSS_PATH;?>ie6.css" rel="stylesheet" type="text/css" />
        <![endif]-->
        <?php
        // Load all scripts 
        if($scripts) {
            foreach($scripts as $script) {
                echo '<script type="'.$script["type"].'" src="'. PROJECT_ROOT . JS_PATH . $script["src"].'"></script>';
            }        
        }
        ?>
    </head>
    <body>
    <?php if($hasHeader) include INCLUDE_PATH . "top.php"; ?>
    <?php include INCLUDE_PATH . "left.php"; ?>