<!-- start header -->
<div id="header">
	<div id="logo">
		<h1><a href="<?php echo PROJECT_ROOT; ?>"><span>yakupkutlu</span>.com</a></h1>
		<p>Proje & Ödev Yükleme sistemi</p>
	</div>
	<div id="menu">
		<ul id="main">
                    <?php foreach($menu as $item) { ?>
                       <li <?php if(isset($item["current"])) { ?> class="current_page_item" <?php } ?>>
                       <a href="<?php echo $item["href"]; ?>"><?php echo $item["title"]; ?></a></li>
                   <?php } ?>
		</ul>
	</div>
	
</div>
<!-- end header -->
<div id="wrapper">
	<!-- start page -->
	<div id="page">