<?php if($hasRight) { ?>
</div>
<!-- end content -->
<!-- start sidebars -->
<div id="sidebar2" class="sidebar">
    <ul>
        <?php if(!$session->id) { ?>
        <li>
                <div>
                    <div class="login-box">
                    <h2>Giriş</h2>                        
                        <form name="login-form" id="login-form" method="post" action="<?php echo PROJECT_ROOT . 'login/'; ?>">
                        <input type="text" placeholder="Kullanıcı Adı" name="kullanici_adi" />
                        <input type="password" placeholder="Parola" name="parola" />
                        <input type="button" name="login" id="login" value="Giriş Yap" />
                        <a href="<?php echo PROJECT_ROOT . 'register/'; ?>" id="register-link">Kayıt Ol</a>                        
                        </form>
                        <div id="login-message"></div>
                    </div>               
                </div>
        </li>
        <?php } else { ?>
        <?php $user = new Kullanici($session->id); ?>
        <li>
            <div>
                <h2>Hoşgeldiniz, <?php echo $user->data->ad_soyad; ?></h2>
                <ul>
                    <li><a href="<?php echo PROJECT_ROOT; ?>profile/user/<?php echo $user->data->kullanici_adi; ?>">@<?php echo $user->data->kullanici_adi; ?></a></li>
                    <li><a href="<?php echo PROJECT_ROOT; ?>logout/">Çıkış</a></li>                    
                </ul>
            </div>
        </li>
        <?php } ?>
    </ul>
    </div>
<!-- end sidebars -->
<div style="clear: both;">&nbsp;</div>
<?php } else { ?>
</div>
<!-- end content -->
<!-- start sidebars -->
<div id="sidebar2" class="sidebar"></div>
<!-- end sidebars -->
<div style="clear: both;">&nbsp;</div>
<?php } ?>
