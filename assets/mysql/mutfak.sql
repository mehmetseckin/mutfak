-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2014 at 11:58 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `mutfak`
--

-- --------------------------------------------------------

--
-- Table structure for table `bolumler`
--

DROP TABLE IF EXISTS `bolumler`;
CREATE TABLE IF NOT EXISTS `bolumler` (
  `bolum_kodu` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `bolum_adi` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`bolum_kodu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bolumler`
--

INSERT INTO `bolumler` (`bolum_kodu`, `bolum_adi`) VALUES
('1501', 'ELEKTRİK ELEKTRONİK MÜHENDİSLİĞİ'),
('1502', 'İNŞAAT MÜHENDİSLİĞİ'),
('1503', 'BİLGİSAYAR MÜHENDİSLİĞİ'),
('1504', 'MAKİNA MÜHENDİSLİĞİ'),
('1506', 'PETROL VE DOĞALGAZ MÜHENDİSLİĞİ');

-- --------------------------------------------------------

--
-- Table structure for table `danisman_bilgiler`
--

DROP TABLE IF EXISTS `danisman_bilgiler`;
CREATE TABLE IF NOT EXISTS `danisman_bilgiler` (
  `kullanici_id` int(11) NOT NULL,
  `unvan` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bolum_kodu` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`kullanici_id`),
  KEY `fk_danisman_bilgiler_bolumler1_idx` (`bolum_kodu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `danisman_bilgiler`
--

INSERT INTO `danisman_bilgiler` (`kullanici_id`, `unvan`, `bolum_kodu`) VALUES
(2, 'Yrd. Doç. Dr.', '1503');

-- --------------------------------------------------------

--
-- Table structure for table `dersler`
--

DROP TABLE IF EXISTS `dersler`;
CREATE TABLE IF NOT EXISTS `dersler` (
  `ders_kodu` int(11) NOT NULL,
  `bolum_kodu` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `danisman_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ders_adi` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ders_kodu`),
  KEY `fk_dersler_bolumler1_idx` (`bolum_kodu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dersler`
--

INSERT INTO `dersler` (`ders_kodu`, `bolum_kodu`, `danisman_id`, `ders_adi`) VALUES
(503, '1503', NULL, 'INTERNET PROGRAMLAMA I'),
(505, '1503', NULL, 'VERİTABANI SİSTEMLERİ'),
(604, '1503', NULL, 'İNTERNET PROGRAMLAMA II'),
(709, '1503', NULL, 'MOBİL UYGULAMA GELİŞTİRME');

-- --------------------------------------------------------

--
-- Table structure for table `gruplar`
--

DROP TABLE IF EXISTS `gruplar`;
CREATE TABLE IF NOT EXISTS `gruplar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grup_adi` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Grup adi',
  `grup_aciklama` text COLLATE utf8_unicode_ci COMMENT 'Grup aciklamasi',
  `aktif` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `gruplar`
--

INSERT INTO `gruplar` (`id`, `grup_adi`, `grup_aciklama`, `aktif`) VALUES
(1, 'Şefler', 'Mutfağın işlevsel, teknik ve bilumum diğer ih', 1),
(2, 'Gurmeler', 'Ders ve projelerin takibini ve değerlendirmes', 1),
(3, 'Aşçılar', 'Projeleri hazırlayan öğrenciler.', 1),
(4, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kategoriler`
--

DROP TABLE IF EXISTS `kategoriler`;
CREATE TABLE IF NOT EXISTS `kategoriler` (
  `kategori_id` int(11) NOT NULL,
  `kategori_adi` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kategori_aciklama` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kategoriler`
--

INSERT INTO `kategoriler` (`kategori_id`, `kategori_adi`, `kategori_aciklama`) VALUES
(1, 'Bitirme Projesi', 'Bitirme Projesi dersi için alınmış olan proje'),
(2, 'Ders Projesi', 'Bir ders için hazırlanacak olan proje'),
(3, 'Proje Ödevi', 'Dersle ilgili verilen proje ödevi');

-- --------------------------------------------------------

--
-- Table structure for table `kullanicilar`
--

DROP TABLE IF EXISTS `kullanicilar`;
CREATE TABLE IF NOT EXISTS `kullanicilar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isim` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Kullanicinin isim bilgisi',
  `soyisim` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Kullanicinin soyisim bilgisi',
  `kullanici_adi` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Essiz kullanici adi',
  `e_mail` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Kullanicinin e-posta adresi',
  `grup_id` int(11) DEFAULT NULL,
  `parola` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'MD5 ile sifrelenmis parola bilgisi',
  `avatar` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Kullanici avatar resim dosyasinin ismi.',
  `kayit_tarihi` date DEFAULT NULL COMMENT 'Kullanicinin sisteme kayit oldugu tarih',
  `son_gorulme` datetime DEFAULT NULL COMMENT 'Kullanicinin son goruldugu tarih',
  `aktif` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kullanici_adi_UNIQUE` (`kullanici_adi`),
  UNIQUE KEY `e_mail_UNIQUE` (`e_mail`),
  KEY `fk_kullanicilar_gruplar1_idx` (`grup_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kullanicilar`
--

INSERT INTO `kullanicilar` (`id`, `isim`, `soyisim`, `kullanici_adi`, `e_mail`, `grup_id`, `parola`, `avatar`, `kayit_tarihi`, `son_gorulme`, `aktif`) VALUES
(1, 'Mehmet', 'Seçkin', 'seckin92', 'seckin92@gmail.com', 1, '2d9b3ad3b32e0075504a94e2965fc764', NULL, '2013-12-08', '2014-02-10 00:54:07', 1),
(2, 'Yakup', 'Kutlu', 'ykutlu', 'ykutlu@mku.edu.tr', 2, '1725156a348e018d7fd59488bdff18f3', NULL, '2013-12-09', '2014-02-10 00:42:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ogrenci_bilgiler`
--

DROP TABLE IF EXISTS `ogrenci_bilgiler`;
CREATE TABLE IF NOT EXISTS `ogrenci_bilgiler` (
  `kullanici_id` int(11) NOT NULL,
  `ogrenci_no` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bolum_kodu` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`kullanici_id`),
  KEY `fk_ogrenci_bilgiler_bolumler1_idx` (`bolum_kodu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projeler`
--

DROP TABLE IF EXISTS `projeler`;
CREATE TABLE IF NOT EXISTS `projeler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proje_adi` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aciklama` text COLLATE utf8_unicode_ci COMMENT 'Projenin kisa aciklamasi. HTML icerebilir.',
  `kategori` int(11) DEFAULT NULL COMMENT 'Proje ile iliskili kategori id',
  `kullanici_id` int(11) DEFAULT NULL,
  `ders_kodu` int(11) DEFAULT NULL,
  `hash` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Projenin essiz hash kodu. Ayni zamanda proje dosyalarinin tutulacagi server dizininin adidir.',
  `yukleme_tarihi` date DEFAULT NULL,
  `son_guncelleme_tarihi` date DEFAULT NULL,
  `son_guncelleme_notu` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `son_guncelleyen` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ortaklar` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Proje grup halinde alinmis olabilir, proje koordinatoru kullanici_id, digerleri ortaklar kisminda yer alir. Virgulle ayrilmis kullanici_id dizisidir.',
  `aktif` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hash_UNIQUE` (`hash`),
  KEY `fk_projeler_kategoriler1_idx` (`kategori`),
  KEY `fk_projeler_dersler1_idx` (`ders_kodu`),
  KEY `kullanici_id` (`kullanici_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `danisman_bilgiler`
--
ALTER TABLE `danisman_bilgiler`
  ADD CONSTRAINT `fk_danisman_bilgiler_bolumler1` FOREIGN KEY (`bolum_kodu`) REFERENCES `bolumler` (`bolum_kodu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_danisman_bilgiler_kullanicilar1` FOREIGN KEY (`kullanici_id`) REFERENCES `kullanicilar` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `dersler`
--
ALTER TABLE `dersler`
  ADD CONSTRAINT `fk_dersler_bolumler1` FOREIGN KEY (`bolum_kodu`) REFERENCES `bolumler` (`bolum_kodu`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kullanicilar`
--
ALTER TABLE `kullanicilar`
  ADD CONSTRAINT `fk_kullanicilar_gruplar1` FOREIGN KEY (`grup_id`) REFERENCES `gruplar` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ogrenci_bilgiler`
--
ALTER TABLE `ogrenci_bilgiler`
  ADD CONSTRAINT `fk_ogrenci_bilgiler_bolumler1` FOREIGN KEY (`bolum_kodu`) REFERENCES `bolumler` (`bolum_kodu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ogrenci_bilgiler_kullanicilar1` FOREIGN KEY (`kullanici_id`) REFERENCES `kullanicilar` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `projeler`
--
ALTER TABLE `projeler`
  ADD CONSTRAINT `projeler_ibfk_1` FOREIGN KEY (`kullanici_id`) REFERENCES `kullanicilar` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_projeler_dersler1` FOREIGN KEY (`ders_kodu`) REFERENCES `dersler` (`ders_kodu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_projeler_kategoriler1` FOREIGN KEY (`kategori`) REFERENCES `kategoriler` (`kategori_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
