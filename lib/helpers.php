<?php
function base_url($uri = '') {
    return PROJECT_ROOT . $uri;
}
function redirect($uri = '') {
    $url = base_url($uri);
try {
    if (!headers_sent()) {
            // Header'lar gonderilmedi, server-side redirecting !
            header('Location: ' . $url);
            exit;
        } else {
            throw new Exception();
        }
    } catch (Exception $ex) {
    // Headers already sent!! O zaman Javascript?
    echo '<script type="text/javascript">'
    . 'window.location.href="' . $url . '";'
    . '</script>'
    // Javascript disable ise, meta ile yolluyoruz
    . '<noscript>'
    . '<meta http-equiv="refresh" content="0;url=' . $url . '" />'
    . '</noscript>';
    exit;
    }    
}

function alert($message, $type = "info") {
    echo '
        <div class="alert alert-'.$type.'">' .
            $message .
        '</div>';    
}