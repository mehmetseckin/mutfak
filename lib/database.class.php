<?php

class Database extends mysqli {
    protected $session;
    public $data;
    public function __construct() {
        parent::__construct('p:'.MySQL_HOST, MySQL_USER, MySQL_PASS, MySQL_DATABASE);
        $this->set_charset(MySQL_CHARACTER_SET);
        
        $this->session = new SessionManager ();
        $this->data = new Registry();        
    }
    
    /**
     * Select sorgusu yapar ve sonuc dondurur.
     * 
     * @param type $table Tablo adi
     * @param type $cols Sutun adlari
     * @param type $join Join yapilacak tablo adi ve yön
     * @param type $where Where şartları
     * @param type $orderBy OrderBy verisi
     * @param type $limit Limit verisi
     * @return boolean false veya result objesi
     */
    protected function select($table, $cols = '*', $join = null, $where = null, $orderBy = null, $limit = null) {
       $q = 'SELECT ' . $cols . ' FROM ' . $table;
       if($join != null) {
           $q .= ' JOIN ' . $join;
       }
       if($where != null) {
           $q .= ' WHERE ' . $where;
       }
       if($orderBy != null) {
           $q .= ' ORDER BY ' . $orderBy;
       }
       if($limit != null) {
           $q .= ' LIMIT ' . $limit ;
       }
       $q .= ';';
       
       loggy("Query : " . $q, "Database");
       $results = $this->query($q);
       if($results !== FALSE) {
           return $results;
       } else {
           return false;
       }
    }
    
    /**
     * Database insert yapan fonksiyon.
     * 
     * @param type $table tablo adi
     * @param array $params parametreler dizisi
     * @return type
     */
    protected function insert($table, $params = array()) {
        $sql='INSERT INTO `'.$table.'` (`'.implode('`, `',array_keys($params)).'`) VALUES (\'' . implode('\', \'', $params) . '\')';               
        
        return ($this->query($sql) !== FALSE) ? true : false;
    }
    
    /**
     * 
     * @param type $table
     * @param type $where
     * @return type
     */
    protected function delete($table, $where = null) {
        if($where == null){
            $delete = 'DELETE '.$table;
        }else{
            $delete = 'DELETE FROM '.$table.' WHERE '.$where;
        }        
        return ($this->query($delete) !== FALSE) ? true : false;        
    }
    /**
     * 
     * @param type $table
     * @param type $params
     * @param type $where
     * @return type
     */
    protected function update($table, $params = array(), $where = null) {
        $args = array();
        foreach ($params as $field => $value) {
            $args[] = $field . '="' . $value . '"';
        }
        $sql = 'UPDATE ' . $table . ' SET ' . implode(',', $args) . ' WHERE ' . $where;
        return ($this->query($sql) !== FALSE) ? true : false;
    }
    
    /**
     * 
     * @param type $value
     * @param type $field
     * @param type $table
     * @return type
     */
    public function exists($value, $field = "id", $table = "kullanicilar") {
        $result = $this->query("SELECT EXISTS(SELECT $field FROM $table WHERE $field = '$value') as \"x\"")->fetch_assoc();
        return ($result["x"]) ? true : false;
    }    
}
