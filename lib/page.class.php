<?php
/**
 * A generic Page controller.
 *
 * @author Mehmet Seckin
 */
class Page extends Registry {
   // An array to store data.
   private $view;
   public function __construct($controller = "default") {
       $bundleFile = CONTROLLER_PATH . $controller . ".php";
       if(!file_exists($bundleFile)) {
            $bundleFile = CONTROLLER_PATH . "default" . ".php";           
       }
       $viewFile = VIEW_PATH . $controller . ".php";
       if(!file_exists($viewFile)) {
           throw new Exception("Cannot find view : $controller", 404);
           exit;
       }
       
       require($bundleFile);
       $this->data = array_merge($this->data, $_);
       $this->view = $viewFile;
   }
   
   public function get($key) {
       return (isset($_GET[$key])) ? $_GET[$key] : null;
   }
   
   public function post($key) {
       return (isset($_POST[$key])) ? $_POST[$key] : null;
   }
   
   public function render() {
       extract($this->data);
       $page = $this;
       if(isset($ajax)) {
           include $this->view;
           return null;
       }
       
       include INCLUDE_PATH . "header.php";
       
       include($this->view);
       
        include INCLUDE_PATH . "footer.php";
   }
}
