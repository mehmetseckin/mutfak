<?php

class Registry {
    protected $data = array();
    
   // Overriding the magic get accessor to respond our arbitrary variable calls.
   public function &__get($varName){
       if (!array_key_exists($varName, $this->data)) {
            $result = false;
            return $result;
        } else {
            $result = $this->data[$varName];
            return $result;
        }
    }

   // Overriding the magic set accessor so it lets us use arbitrary variables.
   public function __set($varName,$value){
      $this->data[$varName] = $value;    
   }
   
    public function import($incomingData = array()) {
       $this->data = array_merge($this->data, $incomingData);
    }
    
    public function all() {
        return $this->data;
    }
}