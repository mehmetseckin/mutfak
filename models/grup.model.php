<?php

class Grup extends Database {
    protected $table = "gruplar";
    
    public function __construct($grup_id = null) {
        parent::__construct();
        
        if($grup_id) {
            $grup_id = $this->real_escape_string($grup_id);
            $info = $this->select($this->table,
                    "*",
                    null,
                    "grup_id = $grup_id",
                    null,
                    1);
            if($info) {
                $this->data->id = $info["grup_id"];
                $this->data->grup_adi = $info["grup_adi"];
                $this->data->aciklama = $info["grup_aciklama"];
            }
        }
    }
    
    public function getList() {
        return $this->select($this->table);
    }
    
    public function get($value, $field = "id") {
        $value = $this->real_escape_string($value);
        $field = $this->real_escape_string($field);
        if(!$this->exists($value, $field, $this->table)) {
            return false;
        }        
        $result = $this->select($this->table,
                 "*",
                 null,
                 "$field = '$value'",
                 null,
                 1)->fetch_assoc();
        if($result) {
            $this->data->id = $result["id"];
            $this->data->grup_adi = $result["grup_adi"];
            $this->data->aciklama = $result["grup_aciklama"];        
        }
        return $result;
    }
}