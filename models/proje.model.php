<?php

class Proje extends Database {
    protected $table = "projeler";
    public function __construct($id = null) {
        parent::__construct();
        
        
    }
    
    public function get($value, $field = "id") {
        $value = $this->real_escape_string($value);
        $field = $this->real_escape_string($field);
        if(!$this->exists($value, $field, $this->table)) {
            return false;
        }        
        
        
    } 
}