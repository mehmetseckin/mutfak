<?php

class Kullanici extends Database {
    protected $table = "kullanicilar";
    
    public function __construct($id = null) {
        // Calling the parent constructor.
        parent::__construct();
        if($id) {
            $id = $this->real_escape_string($id);
            // This is the current logged-in user.
            // Importing all session data into this object.
            $this->data->import($this->session->all());
        }                
    }
    
    /**
     * Istenen degere gore veritabanindan kullanici bilgilerini cekip nesnenin
     * ozelliklerini ayarlar.
     * 
     * @param type $value
     * @param type $field
     * @return boolean
     */
     public function get($value, $field = "id") {
        $value = $this->real_escape_string($value);
        $field = $this->real_escape_string($field);
        if(!$this->exists($value, $field, $this->table)) {
            return false;
        }
        
        $result = $this->select("$this->table k, gruplar g", 
                '*', 
                null, 
                "k.grup_id = g.id"
                . " AND k.$field='$value'"
                . " AND k.aktif=1"
                . " AND g.aktif=1")->fetch_assoc();
        $this->data->id = $result["id"];
        $this->data->kullanici_adi = $result["kullanici_adi"];
        $this->data->grup_id = $result["grup_id"];
        $this->data->grup_adi = $result["grup_adi"];
        $this->data->grup_aciklama = $result["grup_aciklama"];
        $this->data->avatar = $result["avatar"]; 
        $this->data->isim = $result["isim"];
        $this->data->soyisim = $result["soyisim"];
        $this->data->e_mail = $result["e_mail"];
        $this->data->ad_soyad = $this->data->isim . " " . $this->data->soyisim;
        $this->data->son_gorulme = $result["son_gorulme"];
        $this->data->kayit_tarihi = $result["kayit_tarihi"];
        
        if($this->query("SELECT EXISTS(SELECT k.id FROM $this->table k, ogrenci_bilgiler ob WHERE k.id = ob.kullanici_id AND k.id = ".$this->data->id.")")) {
            // Bu kullanici bir ogrenci.
            $result = $this->select("$this->table k, ogrenci_bilgiler ob",
                    'ob.ogrenci_no',
                    null,
                    "k.id = ob.kullanici_id"
                    . " AND k.id=".$this->data->id)->fetch_assoc();
            $this->data->ogrenci_no = $result["ogrenci_no"];
        }       
        if($this->query("SELECT EXISTS(SELECT k.id FROM $this->table k, danisman_bilgiler db WHERE k.id = db.kullanici_id AND k.id = ".$this->data->id.")")) {
            // Bu kullanici bir hoca.
            $result = $this->select("$this->table k, danisman_bilgiler db",
                    'db.unvan',
                    null,
                    "k.id = db.kullanici_id"
                    . " AND k.id=".$this->data->id)->fetch_assoc();
            $this->data->unvan = $result["unvan"];
        }
        
        return $result;
    }
    
    /**
     * Verilen kullanici adi ve parola bilgisi ile giris kontrolu yapar, giris
     * basariliysa nesneyi hazirlar.
     * 
     * @param type $kullanici_adi
     * @param type $parola
     * @return boolean
     */
    public function login($kullanici_adi, $parola) {
        $kullanici_adi = $this->real_escape_string($kullanici_adi);
        $parola = $this->real_escape_string($parola);
        
        if(!$this->exists($kullanici_adi, "kullanici_adi", $this->table)) {
            return false;
        }
        
        $result = $this->select("kullanicilar k, gruplar g", 
                '*', 
                null, 
                "k.grup_id = g.id"
                . " AND kullanici_adi='$kullanici_adi'"
                . " AND parola='$parola'"
                . " AND k.aktif=1"
                . " AND g.aktif=1")->fetch_assoc();
        if($result) {
            $this->updateLastSeen();
            $this->get($kullanici_adi, "kullanici_adi");
            $this->session->import($this->data->all());       
            return true;
        }
        else {
            return false;
        }
    }
    
    public function updateLastSeen() {
        $son_gorulme = date("Y-m-d H:i:s");
        $this->update("kullanicilar", array("son_gorulme" => $son_gorulme), "kullanici_adi = '".$this->data->kullanici_adi."'");        
    }
    
    public function register() {
        if($this->exists($this->data->kullanici_adi, "kullanici_adi", $this->table)) {
            alert("Bu kullanıcı adı daha önce alınmış.");
            return false;
        }
        if($this->exists($this->data->e_mail, "e_mail", $this->table)) {
            alert("Bu e-mail adresine ait bir hesap zaten var!");
            return false;
        }
        if($this->exists($this->data->ogrenci_no, "ogrenci_no", "ogrenci_bilgiler")) {
            alert("Bu öğrenci numarasıyla ilişkili bir hesap zaten var!", "danger");
            return false;            
        }
        
        $stmt = $this->prepare("INSERT INTO $this->table (kullanici_adi, isim, soyisim, e_mail, grup_id, parola, kayit_tarihi, aktif)"
                . " VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
        extract($this->data->all());
        $stmt->bind_param("ssssissi", 
                $kullanici_adi, 
                $isim, 
                $soyisim, 
                $e_mail, 
                $grup_id,
                $parola, 
                $kayit_tarihi, 
                $aktif);
        $stmt->execute();
        $stmt->close();
        $stmt = $this->prepare("INSERT INTO ogrenci_bilgiler(kullanici_id, ogrenci_no, bolum_kodu)"
                . " VALUES ((SELECT id FROM kullanicilar WHERE kullanici_adi='$kullanici_adi'), ?, ?)");
        
        $stmt->bind_param("ss", 
                $ogrenci_no, 
                $bolum_kodu);
        
        $stmt->execute();
        $stmt->close();        
        return true;
    }
    
    public function logout() {
        $this->session->destroy();
        $this->updateLastSeen();
    }
}