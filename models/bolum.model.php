<?php

class Bolum extends Database {
    protected $table = "bolumler";
    
    public function __construct($bolum_kodu = null) {
        parent::__construct();
        
        if($bolum_kodu) {
            $bolum_kodu = $this->real_escape_string($bolum_kodu);
            $info = $this->select($this->table,
                    "*",
                    null,
                    "bolum_kodu = $bolum_kodu",
                    null,
                    1);
            if($info) {
                $this->data->bolum_kodu = $info["bolum_kodu"];
                $this->data->bolum_adi = $info["bolum_adi"];
            }
        }
    }
    
    public function getList() {
        return $this->select($this->table);
    }
    
    public function get($value, $field = "id") {
        $value = $this->real_escape_string($value);
        $field = $this->real_escape_string($field);
        if(!$this->exists($value, $field, $this->table)) {
            return false;
        }        
    }    
}