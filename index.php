<?php
global $timestamp;
$timestamp = -microtime(true);

include "boot.php";

if(isset($_GET["page"])) 
    $route = $_GET["page"]; 
else 
    $route = "default";

$page = new Page($route);

$page->render();
?>