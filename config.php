<?php
// Path Settings
define("PROJECT_ROOT", "http://localhost/mutfak/");
define("PROJECT_PATH", dirname(__FILE__));
define("THEMES_PATH", "assets/themes/");

if(!isset($theme)) $theme = "mutfak";

define("INCLUDE_PATH", THEMES_PATH .$theme."/inc/");
define("CSS_PATH", THEMES_PATH .$theme."/css/");
define("JS_PATH",  THEMES_PATH .$theme."/js/");
define("IMG_PATH", THEMES_PATH .$theme."/images/");
define("VIEW_PATH", THEMES_PATH .$theme."/views/");
define("CONTROLLER_PATH", THEMES_PATH .$theme."/controllers/");

define("LIB_PATH", "lib/");
define("MODEL_PATH", "models/");
define("LOG_PATH", "lib/loggy/logs/");

// Database Settings
define("MySQL_HOST", "localhost");
define("MySQL_USER", "root");
define("MySQL_PASS", "");
define("MySQL_DATABASE", "mutfak");
define("MySQL_PORT", "3336");
define("MySQL_CHARACTER_SET", "utf8");

define("SYSTEM_LOCALE", "Turkish_Turkey.1254");
define("SYSTEM_TIMEZONE", "Europe/Istanbul");
//define("SYSTEM_LOCALE", "tr_TR.UTF8");
